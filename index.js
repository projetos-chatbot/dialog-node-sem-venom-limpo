const express = require("express");
const app = express();
const { WebhookClient } = require("dialogflow-fulfillment");
const { add } = require("lodash");

app.get("/", function (req, res) {
  res.send('<h1 style="color:red;">ESTOU RODANDO NA PORTA 3000<h1>');
});

///////////////////////////////////////// Nome Webhook
app.post("/dialogflow", express.json(), function (req, res) {
  const agent = new WebhookClient({ request: req, response: res });
  console.log("Dialogflow Request headers: " + JSON.stringify(req.headers));
  console.log("Dialogflow Request body: " + JSON.stringify(req.body));

  function welcome(agent) {
    agent.add(`Bem-vindo ao meu agente!`);
  }

  function fallback(agent) {
    agent.add(`Não entendi`);
    agent.add(`Desculpe, você pode tentar de novo?`);
  }

  /////////////////////////// AQUI INSERE AS FUNÇÕES
  function testeok(agent) {
    for (let i = 1; i <= 5; i++) {
      agent.add(`Funcionando Dialog+Webhook!!! ` + i);
    }
  }

  /////////////////////////////////////// intentMaps
  let intentMap = new Map();
  intentMap.set("Default Welcome Intent", welcome);
  intentMap.set("Default Fallback Intent", fallback);
  intentMap.set("testandoWebHook", testeok);

  // intentMap.set('your intent name here', yourFunctionHandler);
  // intentMap.set('your intent name here', googleAssistantHandler);
  agent.handleRequest(intentMap);
});

let port = 3000;
app.listen(port, () => {
  console.log("Executando na porta: " + port);
});
